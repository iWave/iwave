训练mse和perceptual的输入参数解释：
    --input_dir 训练集路径
    --test_input_dir 测试集路径
    --save_model_dir 保存模型的路径。
    --log_dir 保存log的路径 
    --epochs 训练的最大epoch
    --patch_size 训练裁剪的图像的大小
    --batch_size 
    --num_workers 读取数据时的线程数
    --lambda_d 用于调节码率。lambda_d越大，码率越高。
    --scale_list 为初始的量化步长，可以为一个数或一个列表，如--scale_list 8 或者--scale_list 8 7 9。当为一个数时不是可变码率模型，当为列表时为可变码率模型。
				可变码率模型：
					对于13个mse的预训练模型，模型和scale_list的对应关系为：
						0.4_mse.pth:	[8, 7.5, 8.5, 7, 9, 6.5, 9.5],
						0.25_mse.pth:	[8, 7.5, 8.5, 7, 9, 6.5, 9.5],
						0.16_mse.pth:	[8, 7.5, 8.5, 7, 9, 6.5, 9.5],
						0.10_mse.pth:	[16, 15, 17, 14, 18, 13, 19],
						0.0625_mse.pth:	[16, 15, 17, 14, 18, 13, 19],
						0.039_mse.pth:	[16, 15, 18, 14, 20, 13, 22],
						0.024_mse.pth:	[32, 30, 33, 34, 35, 36, 37],
						0.015_mse.pth:	[32, 31, 34, 30, 36, 29, 38],
						0.0095_mse.pth:	[32, 30, 36, 28, 40, 26, 44],
						0.006_mse.pth:	[64, 60, 66, 56, 68, 70, 72],
						0.0037_mse.pth:	[64, 62, 70, 58, 76, 54, 82],
						0.002_mse.pth:	[64, 58, 72, 52, 80, 46, 88],
						0.0012_mse.pth:	[64, 56, 72, 48, 80, 40, 88],
					对于14个perceptual的预训练模型，模型和scale_list的对应关系为：
						0.4_percep.pth:		[8, 7.5, 8.5, 7, 9, 6.5, 9.5],		
						0.25_percep.pth:	[8, 7.5, 8.5, 7, 9, 6.5, 9.5],		
						0.16_percep.pth:	[8, 7.5, 8.5, 7, 9, 6.5, 9.5],		
						0.10_percep.pth: 	[16, 15, 17, 14, 18, 13, 19],		
						0.018_percep.pth:	[16, 15, 17, 14, 18, 13, 19],		
						0.012_percep.pth: 	[16, 15, 18, 14, 20, 13, 22],		
						0.0075_percep.pth:	[32, 30, 33, 34, 35, 36, 37],		
						0.0048_percep.pth: 	[32, 31, 34, 30, 36, 29, 38],		
						0.0032_percep.pth: 	[32, 30, 36, 28, 40, 26, 44],		
						0.002_percep.pth:	[64, 60, 66, 56, 68, 52, 70],		
						0.00145_percep.pth:	[64, 62, 70, 60, 76, 58, 82],		
						0.0008_percep.pth: 	[64, 58, 72, 52, 80, 46, 88],		
						0.00055_percep.pth:	[64, 56, 72, 48, 80, 40, 88],		
						0.00035_percep.pth:	[64, 56, 72, 48, 80, 40, 88],		
				非可变码率模型：
					取可变码率模型的scale_list第一个值即可。
    --alpha_start 软量化时alpha的起点
    --alpha_end 软量化时最大的alpha
    --load_addi_model 预训练additive wavelet模型的路径。预训练模型的命名规则：对于mse优化的模型为lambda_d+mse.pth，对于percep优化的模型为lambda_d+percep.pth
    --load_affi_model 预训练affine wavelet模型的路径。预训练模型的命名规则：对于mse优化的模型为lambda_d+mse.pth，对于percep优化的模型为lambda_d+percep.pth
    --rate_model 熵编码模块预训练模型的路径
    --post_model 后处理预训练模型的路径
    --wavelet_affine 是否使用affine小波变换（训练mse模型时，高码率段不使用affine小波变换，中低码率段使用affine小波。训练perceptual模型时，由于高码率主观质量很好，高码率段不需要训练perceptual模型，训练中低码率段时使用affine小波）
